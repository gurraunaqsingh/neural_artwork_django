from django.contrib import admin
from django.urls import path, include

from api import views

urlpatterns = [
    path('', views.index),
    path('artwork', views.create_artwork),
    path('behind', views.behind_the_scenes),
]