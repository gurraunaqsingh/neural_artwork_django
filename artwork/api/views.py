from django.shortcuts import render
from django.template import loader
from django.http import HttpResponse, FileResponse

from rest_framework.decorators import api_view, permission_classes
from rest_framework.exceptions import ParseError
from rest_framework.response import Response

from PIL import Image

from utils.Artist import Artist

@api_view(('GET',))
def index(request):
	return Response('Hello world!')


@api_view(('GET', 'POST',))
def create_artwork(request):

	if request.method == 'POST':

		if 'file' not in request.data:
			raise ParseError("Empty content")

		f = request.data['file']
		file_bytes = f.read()

		try:
			pil_image = Image.open(f)
			pil_image.verify()
		except:
			raise ParseError("Unsupported image type")

		artwork_as_base64_string = Artist().create_artwork(Image.open(f), "starry_night")

		template = loader.get_template('view_artwork.html')
		context = { 'artwork_as_base64_string' : artwork_as_base64_string }
		return HttpResponse(template.render(context))

		# return HttpResponse(new_artwork, content_type="image/jpeg")

	if request.method == 'GET':
		template = loader.get_template('index.html')
		context = {}
		return HttpResponse(template.render(context, request))

@api_view(('GET',))
def behind_the_scenes(request):
	if request.method == 'GET':
		template = loader.get_template('behind-the-scenes.html')
		context = {}
		return HttpResponse(template.render(context))
